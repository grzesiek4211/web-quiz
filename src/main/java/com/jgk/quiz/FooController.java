package com.jgk.quiz;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FooController {

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
}
