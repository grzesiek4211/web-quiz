package com.jgk.quiz

import spock.lang.Specification

class FooSpec extends Specification {

    def "Should compare 1 to 1"() {
        given:
        1 == 1
    }
}
